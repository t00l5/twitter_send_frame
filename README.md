# Twitter send frame

[![dependency status](https://deps.rs/repo/gitlab/t00l5/twitter_send_frame/status.svg)](https://deps.rs/repo/gitlab/t00l5/twitter_send_frame)
[![pipeline status](https://gitlab.com/t00l5/twitter_send_frame/badges/master/pipeline.svg)](https://gitlab.com/t00l5/twitter_send_frame)

A Twitter bot to send images from an AWS S3 bucket.

## Requirements

Some env var are required

* `AWS_ACCESS_KEY` Required for working with AWS S3
* `AWS_SECRET_KEY` Required for working with AWS S3
* `TWITTER_SEND_FRAME_CONFIG_FILE` Absolute or relative config file path. See [Config file](#config-file) section for
  more details.
* `TWITTER_CONSUMER_KEY` The consumer key, required for posting on Twitter. Can also be an executable parameter.
* `TWITTER_CONSUMER_SECRET` The consumer secret, required for posting on Twitter. Can also be an executable parameter.

## Usage

twitter_send_frame 0.1.0

USAGE:
twitter_send_frame --twitter-consumer-key <twitter-consumer-key> --twitter-consumer-secret <twitter-consumer-secret>

FLAGS:
-h, --help Prints help information -V, --version Prints version information

OPTIONS:
--twitter-consumer-key <twitter-consumer-key>           [env: TWITTER_CONSUMER_KEY]
--twitter-consumer-secret <twitter-consumer-secret>     [env: TWITTER_CONSUMER_SECRET]

## Config file

```json
{
    // Number of frame to post
    "nb_to_post": 3,
    // Resource directory. Used to store Twitter access token and keep trace of image index.
    "resource_dir": "/home/dwittwer/projects/twitter_send_frame/res",
    // Tmp dir to use to keep images from S3 before posting it. Tis directory is removed after each execution
    "tmp_dir": "/tmp/twitter_send_frame",
    "aws": {
        // AWS bucket to use
        "bucket": "my-bucket",
        // AWS region to use
        "region": "us-east-1",
        // Endpoint to use if AWS like object storage [optional]
        "endpoint": "https://my-custom.endpoint.cloud",
        // object key prefix [optional]
        "key_prefix": "frames/storage/",
        // Object key template. {nb | nb_formatter} must be present and will be replaced with the current image number to post
        "key_template": "FRAME{nb | nb_formatter}.jpg",
        // Padding number for the key. For example if set to 4, current number to 42 with previous template the object key will be "FRAME0042.jpg"
        "key_nb_padding": 4
    },
    "twitter": {
        // The message of the tweet. {nb} must be present and will be replaced by the curren frame number
        "message_template": "My awesome movie \n\nframe {nb} of 122894"
    }
}
```

### With Cargo (recommended)

Run cargo install `twitter_send_frame`. 

To specify log level, add `RUST_LOG` env var.

### From sources

Install the Rust toolchain [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install)  
Download sources on [Gitlab](https://gitlab.com/t00l5/twitter_send_frame)  
Inside the downloaded project run `cargo install --path .` to install the tool or `cargo run` to run it.

## Versioning

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and
use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model.

## Copyright and Licence

Copyright 2022 David Wittwer and Contributors

This project is licensed under GNU GPLv3 license. See [license file](LICENSE)
