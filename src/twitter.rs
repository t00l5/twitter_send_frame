use egg_mode::media::{media_types, set_metadata, upload_media};
use egg_mode::tweet::DraftTweet;
use egg_mode::Token;
use std::path::PathBuf;

use crate::error::{Error, Result};
use crate::resource::load_param;
use crate::{dump_param, Param};

pub async fn post_file(token: &Token, txt: String, path: PathBuf) -> Result<()> {
    info!("Create tweet");
    let mut tweet = DraftTweet::new(txt);

    info!("Uploading media from '{}'", path.display());
    let typ = match path.extension().and_then(|os| os.to_str()).unwrap_or("") {
        "jpg" | "jpeg" => media_types::image_jpg(),
        "gif" => media_types::image_gif(),
        "png" => media_types::image_png(),
        "webp" => media_types::image_webp(),
        "mp4" => media_types::video_mp4(),
        _ => {
            error!("Format not recognized, must be one of [jpg, jpeg, gif, png, webp, mp4]");
            std::process::exit(1);
        }
    };
    let bytes = std::fs::read(path.clone())?;
    let handle = upload_media(&bytes, &typ, token).await?;
    tweet.add_media(handle.id.clone());

    set_metadata(
        &handle.id,
        path.file_name().unwrap().to_str().unwrap(),
        token,
    )
    .await?;
    info!("Media uploaded");

    tweet.send(token).await?;
    info!("Tweet sent");

    Ok(())
}

pub async fn get_token(consumer_key: String, consumer_secret: String) -> Result<Token> {
    let con_token = egg_mode::KeyPair::new(consumer_key, consumer_secret);

    let param = load_param()?;
    let token: egg_mode::Token = if let Some(access_token) = param.twitter_token {
        let token = egg_mode::Token::Access {
            consumer: con_token,
            access: access_token,
        };

        if let Err(err) = egg_mode::auth::verify_tokens(&token).await {
            error!("We've hit an error using your old tokens: {:?}", err);
            info!("We'll have to re-authenticate before continuing.");
            let new_param = Param {
                index: param.index,
                twitter_token: None,
            };
            dump_param(&new_param)?;
            return Err(Error::new(
                "We'll have to re-authenticate before continuing.".to_string(),
            ));
        } else {
            info!("Welcome back!");
            token
        }
    } else {
        let request_token = egg_mode::auth::request_token(&con_token, "oob").await?;

        println!("Go to the following URL, sign in, and give me the PIN that comes back:");
        println!("{}", egg_mode::auth::authorize_url(&request_token));

        let mut pin = String::new();
        std::io::stdin().read_line(&mut pin).unwrap();

        let (token, _, screen_name) =
            egg_mode::auth::access_token(con_token, &request_token, pin).await?;

        match token {
            egg_mode::Token::Access {
                access: ref access_token,
                ..
            } => {
                let new_param = Param {
                    index: param.index,
                    twitter_token: Some(access_token.clone()),
                };
                dump_param(&new_param)?;
            }
            _ => unreachable!(),
        }

        info!("Welcome, {}, let's get this show on the road!", screen_name);
        token
    };

    Ok(token)
}
