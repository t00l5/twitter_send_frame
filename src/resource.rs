use crate::config::CONFIG;
use crate::error::Result;
use egg_mode::KeyPair;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::{BufReader, Write};

#[derive(Debug, Serialize, Deserialize)]
pub struct Param {
    pub index: u64,
    pub twitter_token: Option<KeyPair>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Twitter {
    pub access_key: String,
    pub access_secret: String,
}

pub fn dump_param(param: &Param) -> Result<()> {
    let mut file_path = CONFIG.resource_dir.clone();
    file_path.push("param.json");
    let mut file = File::create(file_path)?;
    file.write_all(serde_json::to_string(param)?.as_bytes())?;
    Ok(())
}

pub fn load_param() -> Result<Param> {
    let mut file_path = CONFIG.resource_dir.clone();
    file_path.push("param.json");
    let file = File::open(&file_path)?;
    let reader = BufReader::new(file);

    Ok(serde_json::from_reader(reader)?)
}
