use egg_mode::error::MediaError;
use rusoto_core::RusotoError;
use std::fmt::{Display, Formatter};
use std::num::ParseIntError;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub struct Error {
    msg: String,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.msg.as_str())
    }
}

impl std::error::Error for Error {}

impl Error {
    pub fn new(msg: String) -> Self {
        Error { msg }
    }
}

impl From<ParseIntError> for Error {
    fn from(e: ParseIntError) -> Self {
        Error::new(e.to_string())
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::new(e.to_string())
    }
}

impl From<egg_mode::error::Error> for Error {
    fn from(e: egg_mode::error::Error) -> Self {
        Error::new(e.to_string())
    }
}

impl From<MediaError> for Error {
    fn from(e: MediaError) -> Self {
        Error::new(e.to_string())
    }
}

impl<T: std::error::Error + 'static> From<RusotoError<T>> for Error {
    fn from(e: RusotoError<T>) -> Self {
        Error::new(format!("{}", e))
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::new(e.to_string())
    }
}
