extern crate dotenv;
extern crate env_logger;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate rusoto_s3;

use std::error::Error;
use std::fmt::Write;
use std::str::FromStr;

use pad::{Alignment, PadStr};
use rusoto_core::Region;
use rusoto_s3::S3Client;
use serde::Serialize;
use serde_json::Value;
use structopt::StructOpt;
use tinytemplate::TinyTemplate;

use crate::aws_s3::{get_object, rm_object};
use crate::config::CONFIG;
use crate::resource::{dump_param, load_param, Param};
use crate::twitter::{get_token, post_file};

mod aws_s3;
mod config;
mod error;
mod resource;
mod twitter;

#[derive(Debug, StructOpt)]
#[structopt(name = "twitter_send_frame")]
struct Opt {
    #[structopt(long, env, hide_env_values = true)]
    twitter_consumer_key: String,
    #[structopt(long, env, hide_env_values = true)]
    twitter_consumer_secret: String,
}

#[derive(Serialize)]
struct TtCtx {
    nb: u64,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // This will load the environment variables located at `./.env`, relative to
    dotenv::dotenv().ok();

    // Initialize the logger to use environment variables.
    env_logger::init();

    let opt = Opt::from_args();

    let padding = CONFIG.aws.key_nb_padding;
    let formatter = move |val: &Value, str: &mut String| {
        str.write_str(
            val.to_string()
                .pad(padding, '0', Alignment::Right, false)
                .as_str(),
        )?;
        Ok(())
    };
    let region = match &CONFIG.aws.endpoint {
        Some(e) => Region::Custom {
            name: CONFIG.aws.region.to_owned(),
            endpoint: e.to_owned(),
        },
        None => Region::from_str(CONFIG.aws.region.as_str())?,
    };
    let s3_client = S3Client::new(region);
    let token = get_token(opt.twitter_consumer_key, opt.twitter_consumer_secret).await?;
    let mut tt = TinyTemplate::new();

    tt.add_template("s3_key", CONFIG.aws.key_template.as_str())?;
    tt.add_template("tweet_txt", CONFIG.twitter.message_template.as_str())?;
    tt.add_formatter("nb_formatter", formatter);

    let param = load_param()?;
    let end_index = param.index + CONFIG.nb_to_post as u64;
    debug!(
        "start posting {} tweets from {}, to {}.",
        CONFIG.nb_to_post, param.index, end_index
    );
    for idx in param.index..end_index as u64 {
        let ctx = TtCtx { nb: idx };
        let object_key: String = match &CONFIG.aws.key_prefix {
            Some(p) => format!("{}{}", p, tt.render("s3_key", &ctx)?),
            None => tt.render("s3_key", &ctx)?,
        };
        debug!("get and post {}", object_key);

        let file = get_object(
            &s3_client,
            &CONFIG.tmp_dir,
            CONFIG.aws.bucket.clone(),
            object_key.clone(),
        )
        .await?;

        let text = tt.render("tweet_txt", &ctx)?;
        post_file(&token, text, file).await?;
        rm_object(&s3_client, CONFIG.aws.bucket.clone(), object_key).await?;
    }
    debug!("Posting finished, cleanup and update.");

    std::fs::remove_dir_all(&CONFIG.tmp_dir)?;
    let new_param = Param {
        index: end_index,
        ..param
    };
    dump_param(&new_param)?;

    info!("Tweets posted.");
    Ok(())
}
