use std::fs;
use std::path::{Path, PathBuf};

use rusoto_s3::{DeleteObjectRequest, GetObjectRequest, S3Client, S3};
use tokio::fs::File;
use tokio::io;

use super::error::Error;

pub async fn get_object(
    s3_client: &S3Client,
    storage_dir: &Path,
    bucket: String,
    key: String,
) -> Result<PathBuf, Error> {
    let mut object = s3_client
        .get_object(GetObjectRequest {
            bucket,
            key: key.clone(),
            ..Default::default()
        })
        .await?;

    let body = object.body.take().expect("The object has no body");
    debug!("Object {} fetched", key);

    let mut file_path: PathBuf = storage_dir.to_path_buf();
    if !file_path.exists() {
        fs::create_dir(file_path.clone())?;
    }
    file_path.push(key.split('/').last().unwrap());
    debug!("Object stored in {}", file_path.to_str().unwrap());

    let mut body = body.into_async_read();
    let mut file = File::create(file_path.clone()).await?;
    io::copy(&mut body, &mut file).await?;

    Ok(file_path)
}

pub async fn rm_object(s3_client: &S3Client, bucket: String, key: String) -> Result<(), Error> {
    debug!("{} delete", key);
    s3_client
        .delete_object(DeleteObjectRequest {
            bucket,
            key,
            ..Default::default()
        })
        .await?;
    debug!("Object deleted");

    Ok(())
}
