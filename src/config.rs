use std::env;
use std::path::PathBuf;

use hocon::HoconLoader;
use serde::{Deserialize, Serialize};

lazy_static! {
    pub static ref CONFIG: Config = get_config();
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub aws: Aws,
    pub nb_to_post: u8,
    pub resource_dir: PathBuf,
    pub tmp_dir: PathBuf,
    pub twitter: Twitter,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Aws {
    pub bucket: String,
    pub endpoint: Option<String>,
    pub key_nb_padding: usize,
    pub key_prefix: Option<String>,
    pub key_template: String,
    pub region: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Twitter {
    pub message_template: String,
}

fn get_config() -> Config {
    match env::var("TWITTER_SEND_FRAME_CONFIG_FILE") {
        Ok(val) => HoconLoader::new()
            .load_file(val)
            .expect("Config load error")
            .resolve()
            .expect("Config deserialize error"),
        Err(_) => panic!("Missing env var 'twitter_send_frame_config_file'"),
    }
}
