# Build image
# To work, this image need DISCORD_TOKEN and DATABASE_URL env var.
FROM rust:latest as build

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #
#                                                 ARGUMENTS                                                            #
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #

ARG VERSION
ARG RELEASE_DATE

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #
#                                                 LABELS                                                               #
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #

LABEL vendor="dawen"
LABEL ch.dawen.twitter_send_frame.version=${VERSION}
LABEL ch.dawen.twitter_send_frame.release-date=${RELEASE_DATE}

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #
#                                                 INSTRUCTIONS                                                         #
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #

ENV APP_ENV=prod
ENV RUST_LOG="error,twitter_send_frame=info"

WORKDIR /tmp/code/

COPY src/ /tmp/code/src/
COPY Cargo.toml /tmp/code/
COPY Cargo.lock /tmp/code/
COPY diesel.toml /tmp/code/

RUN cargo install --path .
RUN cargo clean

ENTRYPOINT twitter_send_frame
