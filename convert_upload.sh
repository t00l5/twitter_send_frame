#!/usr/bin/env bash
set -e

for tiff in *.tif; do
    jpg="$(basename "$tiff" .tif).jpg"
    echo "Converting $tiff to $jpg"
    convert "$tiff" "$jpg"
    echo "Upload $jpg"
    swift upload everyframe-g5 "$jpg"
    echo "Remove $tiff"
    rm "$tiff"
    sleep 5
done
